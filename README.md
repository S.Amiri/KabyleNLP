# KabyleNLP
Natural language processing for the kabyle language

## for the Pos tag
Deux algorithmes principaux: 

- le premier, générera uniquement le modèle d'apprentissage. Ce qui permettra de le ré-exécuter pour générer plusieurs modèles à partir de plusieurs corpus étiquetés manuellement et les sauvegarder sur le disque. Nous pourrons ensuite partager ces modèles en lignes pour les besoins des solutions. 
- Le deuxième est l'algorithme qui permettra de charger un modèle pour étiqueter un texte nouveau.
- Correction et amélioration du corpus.
- Amélioration de l'algorithme de tokenization intégré lors de l’exécution de l'algorithme d'étiquetage.
- mise à jour du corpus des affixes pour les besoins de tokenization (segmentation.

Lors du POS tag: à partir du fichier brut_text.txt, l'algorithme d'étiquetage générera deux fichiers: Un fichier segmenté non étiqueté et un autre étiqueté.
_________________

### How to cotribute in Pos tag :

Download the tager, create your own corpus, test it then send comment here.

## Speech recognition & Speech Synthesis

## Tokenization

## Syllabation

## Lemmatization & stemming

## Grammar analysis

## Spell Check

## Statistics & Data Visualization

